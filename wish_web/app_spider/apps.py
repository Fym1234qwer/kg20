from django.apps import AppConfig


class AppSpiderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_spider'
