#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "FymwomanFeng";

'''
数据处理练习
'''

import numpy as np;
import pandas as pd;
from pandas import Series, DataFrame;
from pyecharts.faker import Faker;
import random;
import numpy as np;
from pyecharts import options as opts;
from pyecharts.charts import Bar, Bar3D, Line, Line3D, Pie, Map, Geo, Funnel, Grid, Tab, Page;


# NumPy test
def numpy_test():
    l1 = list(range(1,10));
    print(l1);
    l2 = list(range(11, 20));
    # 创建一维数组
    a = np.array(l1);
    print(a);
    # 创建二维数组
    a2 = np.array([l1, l2]);
    print(a2);
    a3 = np.array(l1, ndmin=2, dtype=int);
    print(a3);

    a4 = np.arange(1, 10);
    print(a4);
    a5 = np.arange(20).reshape(4,5);
    print(a5);

    a6 = np.empty(shape=[2, 3], dtype=int, order="C");
    print(a6);

    a7 = np.zeros(shape=[2, 3], dtype=int, order="C");
    print(a7);

    print(a7.shape);

    a = np.arange(1, 10);
    print(a);
    print(a[1:5]);
    print(a[1:5:2]);

    a = np.arange(20).reshape(4, 5);
    print(a);
    print(a[1, 2]);
    print(a[[1, 2]]);
    print(a[[1, 2], [3, 1]]);
    print(a[a > 5]);

    a1 = np.arange(1, 5);
    a2 = np.arange(1, 5);
    print(a1 + a2);

    a1 = np.arange(10).reshape(2, 5);
    a2 = np.arange(10).reshape(2, 5);
    print(a1);
    print(a2);
    print(a1 + a2);

    a = np.arange(1, 10);
    print(-np.sort(-a));

    a = np.arange(20).reshape(4, 5);
    print(-np.sort(-a, axis=1));
    print(np.amin(a));
    print(np.average(a));
    print(np.median(a));
    print(np.amin(a, axis=0));
    print(np.amax(a, axis=1));

    print(a.reshape(20).tolist());

def series_test():
    data_list = list(range(1, 9));
    index_list = list("标签%d" % i for i in range(1, 9));
    s = Series(data=data_list, index=index_list);
    print(s);

    # d1 = {"第一季度":12, "第二季度":44, "第三季度":87, "第四季度":54};
    # d2 = {"第一季度":33, "第二季度":22, "第三季度":55, "第四季度":44};
    #
    # s = Series(d1);
    # print(s);

    s["标签9"] = 9;
    print(s);
    s["标签9"] = 99;
    print(s);
    print(s.drop(["标签8", "标签9"]));
    print(s["标签9"]);
    print(s.index.values);
    print(s.values);
    print(s[0:3]);
    print(s["标签1":"标签3"]);

def dataFrame_test():
    data = np.arange(20).reshape(5,4);
    index_list = ["宇宙公司", "银河系公司", "太阳系公司", "地球公司", "中国公司"];
    column_list = ["第一季度", "第二季度", "第三季度", "第四季度"];
    df = DataFrame(data=data, index=index_list, columns=column_list);
    print(df);

    # df["优异"] = (df["第四季度"] > 5);
    # print(df);

    # print(df.drop("优异", axis=1));
    print(df.drop("中国公司", axis=0));

    print(df.shape);

    print(df["第一季度"]);
    print(df[["第一季度", "第二季度"]]);
    print(df.loc["宇宙公司"]);
    print(df.loc[["宇宙公司", "银河系公司"]]);
    print(df.loc[["地球公司", "中国公司"], ["第三季度", "第四季度"]]);
    print(df.index.values);
    print(df.columns.values);

    for row in df.itertuples():
        print(row);
        print(row.Index, row.第三季度);

    print(df.sort_index(axis=1, ascending=True));

    print(df);
    print(df.apply(lambda item: item.max() - item.min(), axis=1));

    print(df.describe());

    df.to_csv("/temp/temp.csv", encoding="utf-8-sig");
    df = pd.read_csv("/temp/temp.csv");
    print(df);
    index_list = df["Unnamed: 0"].values.tolist();
    print(index_list);
    df = df.drop("Unnamed: 0", axis=1);
    column_list = df.columns.values.tolist();
    print(df.values);
    print(DataFrame(data=df.values, index=index_list, columns=column_list));


# Faker
def faker_test():
    # faker
    print(Faker.choose());
    print(Faker.visual_color);
    print(Faker.values(0, 100));
    print(Faker.rand_color());
    print(list(z for z in zip(Faker.choose(), Faker.values(0, 100))));
    print(list((x, y, random.randint(1, 100)) for x in range(1, 10) for y in range(1, 10)));


def line_test():
    Line().add_xaxis(
        xaxis_data=Faker.choose()
    ).add_yaxis(
        series_name="line1",
        y_axis=Faker.values(1, 100),
        itemstyle_opts=opts.ItemStyleOpts(color=Faker.rand_color())
    ).add_yaxis(
        series_name="line2",
        y_axis=Faker.values(1, 100),
        itemstyle_opts=opts.ItemStyleOpts(color=Faker.rand_color())
    ).set_global_opts(
        title_opts=opts.TitleOpts(title="主标题", subtitle="副标题", pos_left="10%"),
        # 设置 series_name 位置
        legend_opts = opts.LegendOpts(pos_left="40%"),
    ).render(
        path="/temp/line.html"
    );

def bar_test():
    Bar().add_xaxis(
        xaxis_data=Faker.choose()
    ).add_yaxis(
        series_name="bar1",
        y_axis=Faker.values(1, 100),
        itemstyle_opts=opts.ItemStyleOpts(color=Faker.rand_color())
    ).add_yaxis(
        series_name="bar2",
        y_axis=Faker.values(1, 100),
        itemstyle_opts=opts.ItemStyleOpts(color=Faker.rand_color())
    ).set_global_opts(
        title_opts=opts.TitleOpts(title="主标题", subtitle="副标题", pos_left="10%"),
        # 设置 series_name 位置
        legend_opts=opts.LegendOpts(pos_left="40%"),
    ).render(
        path="/temp/bar.html"
    );

def pie_test():
    Pie().add(
        series_name="饼图名称",
        data_pair=[list(z) for z in zip(Faker.choose(), Faker.values())],
        center=["50%", "50%"],
        # rosetype="radius",
    ).set_series_opts(
        label_opts=opts.LabelOpts(
            formatter = "{b}: {c}",
            color = Faker.rand_color(),
        )
    ).set_global_opts(
        title_opts=opts.TitleOpts(title="主标题", subtitle="副标题", pos_left="10%"),
        # 设置 series_name 位置
        legend_opts=opts.LegendOpts(pos_left="40%"),
    ).render(
        path="/temp/pie.html"
    );



if __name__ == '__main__':
    series_test();