#!/user/bin/new/env python3
# _*_ coding :utf-8

'''
双色球爬虫
http://kaijiang.zhcw.com/zhcw/html/ssq/list.html
'''
import requests
from bs4 import BeautifulSoup
import re
from entity.Double_Color_Ball import *
from datetime import datetime
import time

headers = {
    "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.62",
    "Cookie":""
}
#获取单页数据
def get_dcb_page_data(url):
    print("单页数据:%s" % url)
    # 向目标网址发送请求,获得响应
    r=requests.get(url,headers=headers)
    # 根据响应状态做处理
    if r.status_code==200:
       # 设置响应编码
        r.encoding=r.apparent_encoding
       # 打印响应文本内容
       # print(r.text)

        # 解析响应的内容
       # 构建bs对象
        bs=BeautifulSoup(markup=r.text,features="html.parser")
        # 获取所有tr标签
        tr_list=bs.find_all(name="tr")
        for index, tr in enumerate(tr_list):
            if index == 0 or index == (len(tr_list) - 1) or index == 1:
                continue
            dcb = Double_Color_Ball(
                issur_code=re.findall('<td align="center">(.*?)</td>', str(tr))[1],
                occur_date=re.findall('<td align="center">(.*?)</td>', str(tr))[0],
                red_ball=" ".join(re.findall('<em class="rr">(.*?)</em>', str(tr))),
                blue_ball=re.findall('<em>(.*?)</em>', str(tr))[0],
                create_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                update_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            )
            insert_(dcb, "issur_code")



# 获取所有数据
def get_dcb_all_data(page_size=153):
    # 生成链接列表
    urls = list("http://kaijiang.zhcw.com/zhcw/html/ssq/list_%d.html" % page for page in range(2, page_size + 1))
    urls.insert(0, "http://kaijiang.zhcw.com/zhcw/html/ssq/list.html")
    print(urls)
    for temp in urls:
        get_dcb_page_data(temp)
        time.sleep(5)


if __name__=='__main__':
    url="http://kaijiang.zhcw.com/zhcw/html/ssq/list.html"
    #get_dcb_page_data(url)
    get_dcb_all_data()