#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "FymwomanFeng";

'''
双色球实体bean
'''

from util.sqlalchemy_util import *;

class Double_Color_Ball(Base):

    # 指定表名
    __tablename__ = "double_color_ball";

    # 创建表的参数
    __table_args__ = {
        "mysql_charset": "utf8"
    };

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False);
    issur_code = Column(String(55));
    occur_date = Column(Date);
    red_ball = Column(String(55));
    blue_ball = Column(String(55));
    recommend = Column(String(55));
    create_date = Column(DateTime);
    update_date = Column(DateTime);

if __name__ == '__main__':
    init_db();
    # dcb = Double_Color_Ball(
    #     issur_code="2023111",
    #     occur_date="2023-08-29 14:14:14",
    #     red_ball="1,2,3,6,13,31",
    #     blue_ball="7",
    #     recommend="1,2,3,14,23,33 11",
    #     create_date="2023-08-29 14:14:14",
    #     update_date="2023-08-29 14:14:14"
    # )
    # insert_(dcb, "issur_code");
    # result = get_all(dcb);
    # print(result);
    # for item in result:
    #     print(item.__dict__);
